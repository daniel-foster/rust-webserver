use crate::auth::auth;
use crate::auth::auth::*;
use crate::database::db::*;
use crate::settings::settings::{Configuration, Config};
use actix_session::Session;
use actix_web::web::Form;
use actix_web::{web, HttpRequest, HttpResponse};
use log::*;
use serde::Deserialize;
use std::time::UNIX_EPOCH;
use std::{fs, time};

pub fn config(cfg: &mut web::ServiceConfig) {
    cfg.service(
        web::resource("/login/login.html")
        .route(web::get().to(get))
        .route(web::post().to(login))
        ).service(
            web::resource("/login/register.html")
            .route(web::get().to(get))
            .route(web::post().to(register))
            ).service(
                web::resource("/login/{filename:.*}")
                .route(web::get().to(get_index))
                );
}

async fn get_index(req: HttpRequest) -> HttpResponse {
    let conf = Configuration::init();
    let root = conf.get(Config::ROOT);
    let http_err = fs::read_to_string(format!("{}{}", root, "/error/404.html")).unwrap();
    let path = req.match_info().query("filename");
    let dot = path.rfind(".").unwrap();
    let ext = path.chars().skip(dot).take(path.len()).collect::<String>();
    let head = match &*ext {
        ".js" => "application/javascript",
        ".css" => "text/css",
        ".html" => "text/html",
        &_ => {
            error!("No match for ext {}", ext);
            return HttpResponse::NotFound().body(http_err)
        }
    };

    let file_format = format!("{}{}", root, req.path());
    let header = format!("{}; charset=utf-8", head);

    match fs::read_to_string(file_format.clone()) {
        Ok(f) => {
            info!("Loading {} from {}", req.path(), file_format);
            HttpResponse::Ok()
            .insert_header(("content-type", header))
            .body(f)
        },
        Err(_) => {
            error!("Cannot load {} from {}", req.path(), file_format);
            HttpResponse::NotFound()
            .insert_header(("content-type", "text/html; charset=utf-8"))
            .body(http_err)
        },
    }
}

pub async fn get(req: HttpRequest) -> HttpResponse {
    let conf = Configuration::init();
    let root = conf.get(Config::ROOT);
    if req.path().eq("/login/login.html") {
        info!("Loading {}", req.path());
        return HttpResponse::Ok().body(fs::read_to_string(format!("{}{}", root, "/login/login.html")).unwrap())
    }
    info!("Loading {}", req.path());
    HttpResponse::Ok().body(fs::read_to_string(format!("{}{}", root, "/login/login.html")).unwrap())
}

async fn login(form: Form<LoginForm>, s: Session, req: HttpRequest) -> HttpResponse {
    let conf = Configuration::init();
    let root = conf.get(Config::ROOT);
    let http_err = fs::read_to_string(format!("{}{}", root, "/error/404.html")).unwrap();
    let path = req.path();
    if  path.eq(&format!("{}{}", root, "/login/login.html")) {
        let usr = match auth::auth(form.0).await {
            Some(usr) => usr,
            None => {
                error!("Login fail");
                return HttpResponse::NotFound().finish();
            }
        };

        println!("USR:::{:?}", usr);

        s.insert("user_id", usr.user_id).unwrap();
        s.insert(
            "time",
            time::SystemTime::now()
            .duration_since(UNIX_EPOCH)
            .unwrap()
            .as_millis(),
            )
            .unwrap();
        s.renew();
        info!("Login success");
        HttpResponse::SeeOther()
            .insert_header(("Location", format!("{}{}", root, "/index.html")))
            .finish()
    } else {
        error!("Error");
        HttpResponse::NotFound().body(http_err)
    }
}
#[derive(Deserialize)]
pub struct CreationForm {
    firstname: String,
    lastname: String,
    email: String,
    username: String,
    password: String,
}

pub async fn register(log: Form<CreationForm>, req: HttpRequest) -> HttpResponse {
    let conf = Configuration::init();
    let root = conf.get(Config::ROOT);
    match login_create(LoginForm { username: log.0.username, password: log.0.password, user_id: None}, UserForm { firstname: log.0.firstname, lastname: log.0.lastname, email: Some(log.0.email) }).await {
        Ok(_) => {
            info!("Loading {}", req.path());
            HttpResponse::SeeOther().insert_header(("Location", format!("{}{}", root, "/login/login.html"))).finish()
        }
            Err(_) => {
            error!("Cannot load {}", req.path());
            HttpResponse::NotFound().finish()
            }
    }
}
