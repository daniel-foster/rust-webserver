use std::time;
use std::time::{Duration, UNIX_EPOCH};
use actix_session::Session;
use log::error;
use regex::Regex;
use crate::database::user::*;
use crate::database::db::*;
use rand::*;
use argon2::Config;

const KEY: [u8; 64] = [0; 64];

pub struct Error;

pub async fn login_create(l: LoginForm, u: UserForm) -> Result<(), diesel::result::Error> {

    error!("before");

    let cap = Regex::new("[A-Z]+?").unwrap();
    let low = Regex::new("[a-z]+?").unwrap();
    let num = Regex::new("\\d+?").unwrap();
    let spec = Regex::new("[!@#$%^&*_]+?").unwrap();

    error!("after");


    println!("{}", cap.is_match(&*l.password));
    println!("{}", low.is_match(&*l.password));
    println!("{}", num.is_match(&*l.password));
    println!("{}", spec.is_match(&*l.password));


    if l.username.len() < 7
        || l.password.len() < 7
            || l.username.contains(" ")
            || !cap.is_match(&*l.password)
            || !low.is_match(&*l.password)
            || !num.is_match(&*l.password)
            || !spec.is_match(&*l.password) {

                println!("ERRORRR");
                return Err(diesel::NotFound)
            }

    if u.firstname.len() > 25
    || u.lastname.len() > 25 {
        return Err(diesel::NotFound)
    }

    let user = u.clone();

    if u.email.is_some() {
        if u.email.unwrap().len() > 50 {
            return Err(diesel::NotFound)
        }
    }

    let user = User::save_new(user).await.unwrap();

    error!("after after");

    error!("after after after");

    let l = &mut l.clone();

    l.user_id = user.user_id;

    Login::save_new(l.hash_pass().expect("Argon Err")).await?;

    Ok(())
}

pub async fn auth(login: LoginForm) -> Option<User> {
    let log = Login::from_username(login.username).await;
    match log {
        None => return None,
        Some(logdb) => {
            let ver = argon2::verify_encoded(&*logdb.password, login.password.as_ref()).unwrap();
            if !ver {
                return None;
            }
            logdb.to_user().await
        }
    }
}

pub async fn check_auth(s: Session) -> Result<i32, Error> {
    let time = match s.get::<u64>("time") {
        Err(_) => return Err(Error),
        Ok(None) => return Err(Error),
        Ok(Some(t)) => t,
    };

    if !Duration::from_millis(time).checked_add(Duration::from_secs(3600)).unwrap().gt(&time::SystemTime::now().duration_since(UNIX_EPOCH).unwrap()) {
        return Err(Error)
    }

    match s.get::<i32>("user_id").unwrap() {
        Some(user) => {
            Ok(user)
        },
        None => {
            Err(Error)
        }
    }
}

impl LoginForm {
    pub fn hash_pass(&mut self) -> Result<Self, argon2::Error> {
        let salt: [u8; 32] = thread_rng().gen();
        let config = Config::default();

        self.password = argon2::hash_encoded(self.password.as_bytes(), &salt, &config)?;

        Ok(self.clone())
    }
}
