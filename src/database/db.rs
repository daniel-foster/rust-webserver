use diesel::*;
use serde::Deserialize;
use super::schema::logins;
use super::schema::logins::username;
use super::schema::users::user_id;
use super::schema::users;
use super::schema::users::dsl::*;
use super::schema::logins::dsl::*;
use super::user::User;
use super::user::Login;

const URL: &str = "database/users.db";

pub async fn establish_connection() -> SqliteConnection {
    SqliteConnection::establish(URL).expect("Error Connecting To Database")
}

#[derive(Debug, Clone, Insertable, Deserialize)]
#[diesel(table_name = super::schema::users)]
pub struct UserForm {
    pub firstname: String,
    pub lastname: String,
    pub email: Option<String>,
}

#[derive(Debug, Clone, Insertable, Deserialize)]
#[diesel(table_name = super::schema::logins)]
pub struct LoginForm {
    pub username: String,
    pub password: String,
    pub user_id: Option<i32>,
}

impl User {
    pub async fn from_id(id: i32) -> Option<Self> {
        let con = &mut establish_connection().await;
        users
            .filter(user_id.eq(id))
            .limit(1)
            .load::<Self>(con)
            .unwrap().get(0).cloned()
    }

    pub async fn from_fname(name: String) -> Option<Self> {
        let con = &mut establish_connection().await;
        users
            .filter(firstname.eq(name))
            .limit(1)
            .load::<Self>(con)
            .unwrap().get(0).cloned()
    }

    pub async fn from_lname(name: String) -> Option<Self> {
        let con = &mut establish_connection().await;
        users
            .filter(lastname.eq(name))
            .limit(1)
            .load::<User>(con)
            .unwrap().get(0).cloned()
    }

    pub async fn save_new(user: UserForm) -> Result<User, result::Error> {
        let con = &mut establish_connection().await;
        insert_into(users::table)
            .values(&user)
            .get_result::<User>(con)
    }
}

impl Login {
    pub async fn from_id(id: i32) -> Option<Self> {
        let con = &mut establish_connection().await;
        logins
            .filter(login_id.eq(id))
            .limit(1)
            .load::<Self>(con)
            .unwrap().get(0).cloned()
    }

    pub async fn to_user(self) -> Option<User> {
        let con = &mut establish_connection().await;
        users
            .filter(user_id.eq(self.user_id))
            .limit(1)
            .load::<User>(con)
            .unwrap().get(0).cloned()
    }

    pub async fn from_username(name: String) -> Option<Self> {
        let con = &mut establish_connection().await;
        logins
            .filter(username.eq(name))
            .limit(1)
            .load::<Self>(con)
            .unwrap().get(0).cloned()
    }

    pub async fn save_new(login: LoginForm) -> Result<Login, result::Error> {
        let con = &mut establish_connection().await;
        insert_into(logins::table)
            .values(&login)
            .get_result::<Login>(con)
    }
}
