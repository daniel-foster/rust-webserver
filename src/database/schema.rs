// @generated automatically by Diesel CLI.

diesel::table! {
    logins (login_id) {
        login_id -> Nullable<Integer>,
        username -> Text,
        password -> Text,
        user_id -> Nullable<Integer>,
    }
}

diesel::table! {
    users (user_id) {
        user_id -> Nullable<Integer>,
        firstname -> Text,
        lastname -> Text,
        email -> Nullable<Text>,
    }
}

diesel::allow_tables_to_appear_in_same_query!(
    logins,
    users,
);
