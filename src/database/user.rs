use serde::Deserialize;
use diesel::Queryable;

#[derive(Deserialize, Debug, Clone, Queryable)]
pub struct User {
    pub user_id: Option<i32>,
    pub firstname: String,
    pub lastname: String,
    pub email: Option<String>,
}

#[derive(Deserialize, Debug, Clone, Queryable)]
pub struct Login {
    pub login_id: Option<i32>,
    pub username: String,
    pub password: String,
    pub user_id: Option<i32>,
}
