use actix_web_actors::ws::Message;
use futures_util::future::Either;
use futures_util::{future, StreamExt};
use log::info;
use serde_json::{json, Value};
use tokio::pin;
use tokio::sync::broadcast::Receiver;
use tokio::sync::broadcast::Sender;
use tokio::time::{interval, Instant};

pub async fn cpu_websocket(
    mut session: actix_ws::Session,
    mut msg_stream: actix_ws::MessageStream,
    (tx, mut rx): (Sender<Value>, Receiver<Value>),
) {
    let mut last_tick = Instant::now();
    let mut fut_one = interval(core::time::Duration::from_secs(10));
    let _ = tx.send(json!({"other": "ws_open"}));
    let data = rx.recv().await.unwrap();
    let to_json = serde_json::to_string(&data).unwrap();
    let mut init = false;
    session.text(to_json).await.unwrap();

    'loo: loop {
        let tick = fut_one.tick();
        let system_ms = rx.recv();

        pin!(tick);
        pin!(system_ms);

        let selection_2 = msg_stream.next();
        let selection_1 = future::select(tick, system_ms);

        match future::select(selection_1, selection_2).await {
            Either::Left((Either::Left(_), _)) => {
                if Instant::now().duration_since(last_tick) > core::time::Duration::from_secs(20) {
                    let _ = tx.send(json!({"other": "ws_close"}));
                    session.clone().close(None).await.unwrap();
                    break;
                }
                let _ = session.ping(b"").await;
            }

            Either::Left((Either::Right(msg), _)) => {
                let val = msg.0.unwrap();

                if val["other"] != Value::Null {
                    match val.get("other").unwrap().as_str().unwrap() {
                        "ws_close" => {
                            session.clone().close(None).await.unwrap();
                            break 'loo
                        },
                        _ => {}
                    }
                } else if val["disk_free"] != Value::Null {
                    if init {
                        continue;
                    } else {
                        init = true;
                        let _ = session.text(serde_json::to_string(&val).unwrap()).await;
                    }
                } else if val["ms"] != Value::Null || val["cores"] != Value::Null {
                    let to_json = serde_json::to_string(&val).unwrap();
                    let _ = session.text(to_json).await;
                }
            }

            Either::Right(msg) => {
                match msg.0 {
                    None => break 'loo,
                    Some(msg) => match msg.unwrap() {
                        Message::Text(txt) => {
                            let json: Value = serde_json::from_str(&txt).unwrap();
                            if json["ms"] != Value::Null {
                                let ms = json["ms"].as_u64().unwrap();
                                if ms < 100 || ms > 10000 {
                                    log::warn!("Invalid message received: {}", ms);
                                    let _ = tx.send(json!({"other": "ws_close"}));
                                    break 'loo;
                                } else {
                                    println!("update");
                                    let _ = tx.send(json!({"ms": ms}));
                                }
                            } else if json["kill"] != Value::Null {
                                let _ = tx.send(json);
                            }
                        }
                        Message::Binary(_) => {}
                        Message::Continuation(_) => {}
                        Message::Ping(_) => {}
                        Message::Pong(_) => {
                            last_tick = Instant::now()
                        }
                        Message::Close(_) =>{
                            let _ = tx.send(json!({"other": "ws_close"}));
                            break 'loo;
                        }
                        Message::Nop => {}
                    },
                }
            }
        }
    }
}
