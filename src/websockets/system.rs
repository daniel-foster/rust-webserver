use std::cmp::Ordering;
use std::path::Path;
use std::time::{Duration, Instant};
use futures_util::future::{Either, select};
use serde_json::{json, Value};
use tokio::pin;
use tokio::sync::broadcast::{Receiver, Sender};
use sysinfo::*;
use tokio::time::{Interval, interval};

// main loop for system refresh.

pub async fn start_system_refresh(sys_bc: (Sender<Value>, Receiver<Value>)) {
    let mut s = System::new_all();
    s.refresh_all();

    let (tx, mut rx) = sys_bc;
    let mut millis = 2000;
    let mut change = 0;
    let mut new_mills = 0;

    let mut instant = Instant::now();

    let mut sockets = 0;

    let mut sigint = tokio::signal::unix::signal(tokio::signal::unix::SignalKind::interrupt()).unwrap();


    loop {
        let mut fut_one: Interval;
        let tick;

        if change == 0 && sockets != 0 {
            fut_one = interval(Duration::from_millis(millis));
            let _ = fut_one.tick().await;
            tick = fut_one.tick();
        } else if instant.elapsed().as_millis() < millis as u128 {
            fut_one = interval(Duration::from_millis(millis - instant.elapsed().as_millis() as u64));
            let _ = fut_one.tick().await;
            tick = fut_one.tick();
        } else if sockets == 0 {
            fut_one = interval(Duration::from_secs_f32((1 * 60 * 60) as f32));
            let _ = fut_one.tick().await;
            tick = fut_one.tick();
        } else {
            instant = Instant::now();
            change = 0;
            millis = new_mills;
            fut_one = interval(Duration::from_millis(millis));
            let _ = fut_one.tick().await;
            tick = fut_one.tick();
        }

        let rx = rx.recv();

        pin!(rx);
        pin!(tick);

        let selection_1 = select(tick, rx);
        let selection_2 = sigint.recv();
        pin!(selection_2);

        match select(selection_1, selection_2).await {
            Either::Left((Either::Left(_), _)) => {
                s.refresh_all();
                let cpus = s.cpus().iter().map(|c| format!("{:.2}", c.cpu_usage())).collect::<Vec<String>>();
                let net = s.networks().iter().find(|(n, _)| n.clone().eq(&String::from("enp5s0"))).unwrap().1;
                let cpu = format!("{:.2}", cpus.iter().map(|c| c.parse::<f32>().unwrap()).sum::<f32>() / cpus.len() as f32);
                let die_tmp = s.components().iter().find(|c| c.label().to_string().eq(&String::from("k10temp Tdie"))).unwrap();
                let fan_tmp = s.components().iter().find(|c| c.label().to_string().eq(&String::from("k10temp Tctl"))).unwrap();
                let mut procs: Vec<Vec<String>> = Vec::new();
                s.processes().iter().for_each(|p| {
                    procs.push(vec![p.0.to_string(),
                    p.1.name().to_string(),
                    String::from(p.1.cwd().to_str().unwrap()),
                    format!("{:.2}", (p.1.cpu_usage() / cpus.len() as f32)),
                    format!("{:.2}", (p.1.memory() / 1000000)),
                    format!("{}", p.1.status().to_string()),
                    ])
                });
                procs.sort_by(|b, a| {
                    if b.get(3).unwrap().parse::<f32>().unwrap() > a.get(3).unwrap().parse::<f32>().unwrap() {
                        Ordering::Less
                    } else if b.get(3).unwrap().parse::<f32>().unwrap() < a.get(3).unwrap().parse::<f32>().unwrap() {
                        Ordering::Greater
                    } else {
                        Ordering::Equal
                    }
                });
                let all = json!({
                    "cores": cpus,
                    "mem_free": s.available_memory() / 1000000,
                    "mem_total": s.total_memory() / 1000000,
                    "cpu_avg": cpu,
                    "net_up": (((net.transmitted() as f32) * 8 as f32) / (millis as f32 / 1000 as f32)),
                    "net_down": (((net.received() as f32) * 8 as f32) / (millis as f32 / 1000 as f32)),
                    "die_tmp": die_tmp.temperature(),
                    "fan_tmp": fan_tmp.temperature(),
                    "procs": procs,
                });
                let _ = tx.send(all);
            }
            Either::Left((Either::Right(ms),_)) => {
                let msg_res = ms.0;

                if msg_res.is_err() {
                    break
                }

                let msg = msg_res.unwrap();

                if msg["ms"] != Value::Null {
                    new_mills = msg["ms"].as_u64().unwrap();
                    let all = json!({
                        "ms": new_mills,
                    });
                    let _ = tx.send(all);
                    change = 1;
                } else if msg["other"] != Value::Null {
                    match msg["other"].as_str().unwrap() {
                        "ws_close" => {
                            sockets -= 1;
                        },
                        "ws_open" => {
                            let cpus = s.cpus().iter().map(|c| format!("{:.2}", c.cpu_usage())).collect::<Vec<String>>();
                            let net = s.networks().iter().find(|(n, _)| n.clone().eq(&String::from("enp5s0"))).unwrap().1;
                            let cpu = format!("{:.2}", cpus.iter().map(|c| c.parse::<f32>().unwrap()).sum::<f32>() / cpus.len() as f32);
                            let die_tmp = s.components().iter().find(|c| c.label().to_string().eq(&String::from("k10temp Tdie"))).unwrap();
                            let fan_tmp = s.components().iter().find(|c| c.label().to_string().eq(&String::from("k10temp Tctl"))).unwrap();
                            let dis = s.disks().iter().find(|d| d.mount_point().eq(Path::new("/"))).unwrap();
                            let procs: Vec<Value> = Vec::new();
                            let all = json!({
                                "ms": millis,
                                "cores": cpus,
                                "mem_free": s.available_memory() / 1000000,
                                "mem_total": s.total_memory() / 1000000,
                                "cpu_avg": cpu,
                                "net_up": (net.transmitted() / 1000000) / (millis / 1000),
                                "net_down": (net.received() / 1000000) / (millis / 1000),
                                "up": s.uptime(),
                                "die_tmp": die_tmp.temperature(),
                                "fan_tmp": fan_tmp.temperature(),
                                "disk_free": dis.available_space() / 1000000000,
                                "disk_percent": (dis.total_space() - dis.available_space()) as f64 / dis.total_space() as f64 * 100 as f64,
                                "disk_total": dis.total_space() / 1000000000,
                                "procs": procs,
                            });
                            tx.send(all).unwrap();
                            sockets += 1;
                        },
                        _ => {}
                    }
                } else if msg["kill"] != Value::Null {
                    s.processes().iter().find(|p| p.0.as_u32().eq(&(msg["kill"].as_u64().unwrap() as u32))).unwrap().1.kill();
                } else {
                    let _ = tx.send(json!({"other": "ws_close" }));
                    sockets -= 1;
                }
            },

            Either::Right(_) => {
                let _ = tx.send(json!({"other": "ws_close" }));
                break;
            }
        };
    };
}
