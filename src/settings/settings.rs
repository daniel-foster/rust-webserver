use std::{process::exit, fs::{File, self}, io::Write};
use super::arg::Arg;
use configparser::ini::Ini;
use log::*;
use rcgen;

#[derive(Debug)]
pub enum Config {
    IP,
    PORT,
    LOGLEVEL,
    ROOT,
    CONFIG,
    SESSIONS,
}

#[derive(Debug, Clone)]
pub struct Configuration {
    ip: String,
    port: i64,
    log_level: String,
    site_root: String,
    config: String,
    sessions: bool,
}

impl Configuration {
    pub(crate) fn init() -> Configuration {
        let args = Arg::new();
        let mut ini = Ini::new();

        let def_conf = String::from("settings.ini");

        let matches = args.get_matches();

        let mut config = matches.opt_str("c")
            .unwrap_or_else(|| {
                def_conf.clone()
            });

        ini.load(&config)
            .unwrap_or_else(|_| {
                config = def_conf.clone();
                ini.load(def_conf.clone()).unwrap_or_else(|_| {
                    println!("Could not read from config at {}", def_conf);
                    args.usage();
                    exit(0)
                })
            });

        let log_level = match &*matches.opt_str("L")
            .unwrap_or_else(|| ini.get("SERVER", "LOG_LEVEL")
                .unwrap_or_else(|| {
                    info!("Defaulting log level Warn");
                    String::from("warn")
                })) {
            "none" => String::from("none"),
            "trace" => String::from("trace"),
            "debug" => String::from("debug"),
            "info" => String::from("info"),
            "warn" => String::from("warn"),
            "error" => String::from("error"),
            _ => {
                error!("Cannot get log level");
                exit(1)
            },
        };

        std::env::set_var("RUST_LOG", &log_level);

        let _ = env_logger::try_init();

        if matches.opt_str("c").is_some() && config == def_conf {
            error!("The config {} could not be read.", matches.opt_str("c").unwrap());
            args.usage();
            exit(0)
        }

        if matches.opt_present("K") {
            let _ = fs::remove_file("key/key.pem");
            let _ = fs::remove_file("key/cert.pem");
            let ip = ini.get("SERVER", "IP").unwrap();
            let subj_alt_names = vec![ip, "localhost".to_string()];
            let gen = rcgen::generate_simple_self_signed(subj_alt_names).unwrap();
            let mut key = File::create("key/key.pem").unwrap();
            let mut cert = File::create("key/cert.pem").unwrap();
            cert.write_all(gen.serialize_pem().unwrap().as_bytes()).unwrap();
            key.write_all(gen.serialize_private_key_pem().as_bytes()).unwrap();
        }

        let ip = matches.opt_str("a")
            .unwrap_or_else(|| ini.get("SERVER", "IP")
                .unwrap_or_else(|| {
                    warn!("Using default localhost");
                    String::from("localhost")
                }));

        let port = matches.opt_str("p")
            .unwrap_or_else(|| ini.get("SERVER", "PORT")
                .unwrap_or_else(|| {
                    warn!("Using default port 8080");
                    String::from("8080")
                }));

        let site_root = matches.opt_str("r")
            .unwrap_or_else(|| ini.get("WEB", "ROOT")
                .unwrap_or_else(|| {
                    error!("Website root not specified");
                    args.usage();
                    exit(1)
                }));

        let sessions: bool = matches.opt_present("S").then(|| false)
            .unwrap_or_else(|| ini.getbool("SERVER", "SESSIONS")
                .unwrap_or_else(|_| {
                    error!("Error reading SESSIONS from {}", config);
                    args.usage();
                    exit(1)
                })
                .unwrap_or(true));

        Configuration {
            ip,
            port: port.parse().expect("Port Must Be a String"),
            log_level,
            site_root,
            config,
            sessions,
        }
    }

    pub fn get(&self, c: Config) -> String {
        match c {
            Config::IP => self.ip.clone(),
            Config::PORT => self.port.to_string().clone(),
            Config::LOGLEVEL => self.log_level.clone(),
            Config::ROOT => self.site_root.clone(),
            Config::CONFIG => self.config.clone(),
            Config::SESSIONS => self.sessions.to_string().clone(),
        }
    }

    pub fn get_bool(&self, c: Config) -> bool {
        match c {
            Config::SESSIONS => self.sessions,
            _ => {
                error!("Cannot get bool from {:?}", c);
                exit(1)
            }
        }
    }
}

