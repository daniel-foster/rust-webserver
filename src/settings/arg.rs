use getopts::{Matches, Options};
use std::env;
use std::process::exit;

pub struct Arg {
    opts: Options,
    program: String,
    matches: Matches,
}

impl Arg {
    pub fn usage(&self) {
        let f = format!("Usage: {} FILE [options]", self.program);
        print!("{}", self.opts.usage(&f));
    }

    fn usage_from_opts(opts: Options, program: String) {
        let f = format!("Usage: {} [options]", program);
        print!("{}", opts.usage(&f));
    }

    pub fn new() -> Self {
        let args: Vec<String> = env::args().collect();
        let program = args[0].clone();
        let mut opts = Options::new();

        opts.optflag("h", "help", "print help menu");
        opts.optflag("s", "admin", "Enables admin web page");
        opts.optflag("S", "sessions", "Disables sessions");
        opts.optflag("K", "regen_ssl", "Regenerate the self signed openssl certificate");
        opts.optopt("c", "config", "Define a server config file", "PATH");
        opts.optopt("a", "address", "Define the IP-address for the server to listen to", "IP");
        opts.optopt("p", "port", "Define the port for the server to use", "PORT");
        opts.optopt("L", "log-level", "Define the log level", "LEVEL");
        opts.optopt("r", "web-root", "Define website root", "PATH");

        let matches = match opts.parse(&args[1..]) {
            Ok(m) => m,
            Err(_) => {
                Self::usage_from_opts(opts, program);
                exit(1);
            }
        };

        if matches.opt_present("h") {
            Self::usage_from_opts(opts, program);
            exit(1);
        }

        Arg {
            opts,
            program,
            matches,
        }
    }

    pub fn get_matches(&self) -> Matches {
        self.matches.clone()
    }
}
