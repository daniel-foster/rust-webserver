use std::fs;
use actix_session::Session;
use actix_web::{HttpRequest, web};
use actix_web::HttpResponse;
use log::*;
use serde_json::Value;
use tokio::sync::broadcast::{Receiver, Sender};
use tokio::task::spawn_local;
use crate::auth::auth::check_auth;
use crate::settings::settings::Configuration;
use crate::websockets::websockets::cpu_websocket;
use crate::settings::settings::Config;

pub async fn cpu_ws_route(req: HttpRequest, stream: web::Payload, sen_rec: (Sender<Value>, Receiver<Value>)) -> HttpResponse {
    info!("Connecting to system websocket");
    let (resp, ses, mss) = actix_ws::handle(&req, stream).unwrap();
    spawn_local(cpu_websocket(ses, mss, sen_rec));
    resp
}

pub async fn get_file(req: HttpRequest, s: Session) -> HttpResponse {
    let conf = Configuration::init();
    let root = conf.get(Config::ROOT);

    if conf.get_bool(Config::SESSIONS) {
        match check_auth(s).await {
            Ok(_) => (),

            Err(_) => {
                info!("Login Redirect");
                return HttpResponse::SeeOther().insert_header(("Location", "/login/login.html")).finish()
            }
        }
    }

    let err_404 = fs::read_to_string(format!("{}/error/404.html", root)).unwrap();
    let path = req.match_info().query("path");
    let dot = path.rfind(".").unwrap();
    let ext = path.chars().skip(dot).take(path.len()).collect::<String>();

    let head = match &*ext {
        ".js" => "application/javascript",
        ".css" => "text/css",
        ".html" => "text/html",
        &_ => return HttpResponse::NotFound().body(err_404)
    };

    let file_format = format!("{}/{}", root, path);
    let header = format!("{}; charset=utf-8", head);

    match fs::read_to_string(file_format.clone()) {
        Ok(f) => {
            info!("Loading {} from {}", req.path(), file_format);
            HttpResponse::Ok().insert_header(("content-type", header)).body(f)
        },
        Err(_) => {
            error!("Cannot load file {} from {}", req.path(), file_format);
            HttpResponse::NotFound().insert_header(("content-type", "text/html; charset=utf-8")).body(err_404)
        },
    }
}

pub async fn get_file_unprot(req: HttpRequest) -> HttpResponse {
    let conf = Configuration::init();
    let root = conf.get(Config::ROOT);
    let err_404 = fs::read_to_string(format!("{}/error/404.html", root)).unwrap();
    let path = req.match_info().query("path");
    let dot = req.match_info().query("path").rfind(".").unwrap();
    let ext = path.chars().skip(dot).take(path.len()).collect::<String>();

    let head = match &*ext {
        ".js" => "application/javascript",
        ".css" => "text/css",
        ".html" => "text/html",
        &_ => return HttpResponse::NotFound().body(err_404)
    };

    let file_format = format!("{}/{}", root, path);
    let header = format!("{}; charset=utf-8", head);

    match fs::read_to_string(file_format.clone()) {
        Ok(f) => {
            info!("Loading {} from {}", req.path(), file_format);
            HttpResponse::Ok().insert_header(("content-type", header)).body(f)
        }
        Err(_) => {
            error!("Cannot load {} from {}", req.path(), file_format);
            HttpResponse::NotFound().insert_header(("content-type", "text/html; charset=utf-8")).body(err_404)
        }
    }
}

pub async fn fallback() -> HttpResponse {
    info!("SEND 404");
    let ff = fs::read_to_string("frontend/error/404.html").unwrap();
    HttpResponse::NotFound().body(ff)
}
