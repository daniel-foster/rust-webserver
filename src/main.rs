mod settings;
mod auth;
mod websockets;
mod admin;
mod handlers;
mod database;

use crate::settings::settings::*;
use crate::websockets::system::*;
use admin::config;

use std::default::Default;

use actix_session::SessionMiddleware;
use actix_session::storage::CookieSessionStore;

use actix_web::{App, HttpServer, web};
use actix_web::cookie::Key;
use actix_web::web::Data;

use openssl::ssl::{SslAcceptor, SslFiletype, SslMethod};

use log::*;
use serde_json::Value;

use tokio::sync::broadcast;
use tokio::sync::broadcast::{Receiver, Sender};


use crate::handlers::*;

#[actix_web::main]
async fn main() -> std::io::Result<()>{
    let secret_key = [0; 64];
    let (ws_sen, ws_rec) = broadcast::channel::<Value>(1);
    let (sys_sen, _) = broadcast::channel::<Value>(1);

    let ssc = sys_sen.clone();
    tokio::spawn(async move {
        start_system_refresh((sys_sen.clone(), ws_rec)).await;
    });

    let mut builder = SslAcceptor::mozilla_intermediate(SslMethod::tls()).unwrap();
    builder
        .set_private_key_file("key/key.pem", SslFiletype::PEM)
        .unwrap();
    builder.set_certificate_chain_file("key/cert.pem").unwrap();

    let conf = &Configuration::init();
    let format_ip = format!("{}:{}", conf.get(Config::IP), conf.get(Config::PORT));
    info!("Starting Server At: {}", format_ip);

    HttpServer::new(move || {
        App::new()
            .wrap(SessionMiddleware::new(
                CookieSessionStore::default(),
                Key::from(&secret_key)))
            .configure(|c| config(c))
            .service(web::resource("/cpuWs")
                .route(web::get().to(|req, stream, d:  Data<(Sender<Value>,  Receiver<Value>)>| async move {
                    let (sen, rec) = d.get_ref();
                    cpu_ws_route(req, stream, (sen.clone(), rec.resubscribe())).await
                }))
                .app_data(Data::new((ws_sen.clone(),ssc.subscribe())))
            )
            .route("/error/{path:.*}", web::get().to(get_file_unprot))
            .route("/{path:.*}", web::get().to(get_file))
            .default_service(web::to(fallback))
    })
        .bind_openssl(format_ip, builder)?
        .run()
        .await

}
